export class App {
  configureRouter(config, router) {
    config.title = 'Forecaster app';
    config.map([
      { route: [''], name: 'forecaster', moduleId: './forecaster', nav: false, title: 'Forecaster' }
    ]);

    this.router = router;
  }
}
