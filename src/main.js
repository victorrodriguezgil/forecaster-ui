// comment out if you don't want a Promise polyfill (remove also from webpack.common.js)
import * as Bluebird from 'bluebird';
// we want font-awesome to load as soon as possible to show the fa-spinner
import '../styles/styles.scss';

Bluebird.config({ warnings: false });

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging();
  return aurelia.start().then(() => aurelia.setRoot('app'));
}
