import { App } from '../../src/app';

class RouterStub {
  configure(handler) {
    handler(this);
  }

  map(routes) {
    this.routes = routes;
  }
}

describe('the App module', () => {
  var app;
  var mockedRouter;

  beforeEach(() => {
    mockedRouter = new RouterStub();
    app = new App();
    app.configureRouter(mockedRouter, mockedRouter);
  });

  it('contains a router property', () => {
    expect(app.router).toBeDefined();
  });

  it('configures the router title', () => {
    expect(app.router.title).toEqual('Forecaster app');
  });

  it('should have a forecaster route', () => {
    expect(app.router.routes).toContain({ route: [''], name: 'forecaster', moduleId: './forecaster', nav: false, title: 'Forecaster' });
  });
});
