const babelRegister = require('babel-register');

exports.config = {
  baseUrl: 'http://localhost:19876/',
  // use `npm run e2e`
  specs: [
    'e2e/**/*.feature'
  ],
  exclude: [],

  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),

  allScriptsTimeout: 110000,

  jasmineNodeOpts: {
    showTiming: true,
    showColors: true,
    isVerbose: true,
    includeStackTrace: false,
    defaultTimeoutInterval: 400000
  },
  directConnect: true,

  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: ['show-fps-counter=true']
    }
  },

  onPrepare: () => {
    process.env.BABEL_ENV = 'e2e';
    babelRegister();
  },

  plugins: [{
    package: 'aurelia-protractor-plugin'
  }],

  cucumberOpts: {
    require: ['e2e/*.steps.js'],
    tags: false,
    format: 'pretty',
    profile: false,
    'no-source': true
  }
};
