import * as poForecaster from './forecaster.po';

module.exports = function stepDefs() {
  this.Given(/^I open the Forecaster page$/, (callback) => {
    browser.loadAndWaitForAureliaPage('http://localhost:19876');
    callback();
  });

  this.Then(/^The logo is present$/, (callback) => {
    browser.isElementPresent(poForecaster.getLogo()).then((result) => {
      if (result) {
        callback();
      } else {
        callback(new Error('Expected to see logo'));
      }
    });
  });
};
