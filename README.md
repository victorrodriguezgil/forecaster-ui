# forecaster-ui

## Installation
Node: tested on version 6.9.2

NPM: tested n version 4.0.5

The tests will pop up a Chrome browser, you need Chrome installed as well.

Check out this repo, then do:
```
npm install
```

For Windows users, it's handy to set up `git` to check out `LF`s instead of `CRLF`s, like this:
```
git config --global core.autocrlf input
```

## Testing
Run unit tests with:
```
npm test
```

and end to end tests with:

```
npm run e2e
```

Run `eslint` with:
```
npm run lint
```
## Running

```
npm start
```

will start the `webpack` dev server and hot-reload your changes.

## Building

Build for production with:
```
npm run build:prod
```
